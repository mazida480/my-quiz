import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.css']
})
export class ProfileHeaderComponent implements OnInit
{
  photoUrl!:string
  displayName!:string

  constructor(private authService:AuthService)
  {
   
  }

  ngOnInit()
  {
    this.authService.currentUser$ 
    this.photoUrl = this.authService.currentUser$.photoURL 
    this.displayName = this.authService.currentUser$.displayName
  }
  logout()
  {
    this.authService.signOut()
    
  }
}

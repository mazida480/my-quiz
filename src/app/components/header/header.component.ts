import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent 
{
  hide:boolean = true

  constructor(private router:Router, private authservice:AuthService )
  {
    console.log(this.authservice.hideLogginHeader)
  }


  loginWithGoogle()
  {
    this.authservice.GoogleAuth() 
    this.authservice.hideLogginHeader.next(false)
    this.hide = this.authservice.hideLogginHeader.value

  }

}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent 
{
  data:any = {}
  constructor(private router:Router, private authService: AuthService)
  {
    
  }

  loginWithGoogle()
  {
    this.authService.GoogleAuth()
  }

  logout()
  {
    this.authService.signOut()
    this.router.navigate(['/home'])

  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { BehaviorSubject, Observable} from 'rxjs';
import { Question } from '../../models/question';


@Injectable({
  providedIn: 'root'
})
export class DataapiService
 {

  public filterVal = new BehaviorSubject<any>({category:"Science", level:"Beginner", qnum:5})
  public score = new BehaviorSubject<any>({})
  public selectedAnswer = new BehaviorSubject<any>([])

  baseUrl = "http://localhost:8080"

  constructor(private httpclient:HttpClient) { }

  // Get all the question from the database. 
  getAllQues():Observable<Question[]>
  {
    return this.httpclient.get<Question[]>(`${this.baseUrl}/questions`)
  }

  getOneQues(id:string):Observable<Question>
  {
    return this.httpclient.get<Question>(`${this.baseUrl}/question/`+id)
  }

  postQues(ques:Question):Observable<{}>
  {
    return this.httpclient.post<{}>(`${this.baseUrl}/question`, ques)
  }

  deleteQues(id:string):Observable<number>
  {
    return this.httpclient.delete<number>(`${this.baseUrl}/delete/`+id)
  }

  updateQues(ques:Question, id:string):Observable<{}>
  {
    return this.httpclient.put<{}>(`${this.baseUrl}/edit/`+id, ques)
  }

  rectifyAns(ques:Question, id:string)
  {
    return this.httpclient.patch(`${this.baseUrl}/editans/`+id, ques )
  }

  filterByCatAndLevel(cat:string, level:string):Observable<Question[]>
  {
    return this.httpclient.get<Question[]>(`${this.baseUrl}/filterquestion/${cat}/${level}`)
  }


}

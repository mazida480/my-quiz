import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAuthProvider} from 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService
{

  public currentUser$:any
  public userGuard!:boolean 
  public adminGuard!:boolean
  public hideLogginHeader = new BehaviorSubject<boolean>(true)
  

  constructor(public afAuth:AngularFireAuth, public router:Router)
  {
  }

  //Sign in with Google.
  GoogleAuth()
  {
    const provider = new GoogleAuthProvider()
    provider.setCustomParameters({prompt:'select_account'})
    return this.AuthLogin(provider)
  }

  //Auth login to run auth provider
  AuthLogin(provider:any)
  {
    
    return this.afAuth.signInWithPopup(provider).then((res:any) =>
      {
        this.currentUser$ = localStorage.setItem('user', JSON.stringify((res.user)))

        this.currentUser$ = JSON.parse(<string>localStorage.getItem('user'))

        if(this.currentUser$.email == 'mazida480@gmail.com')
        {
          this.userGuard = true
         
          this.router.navigate(['/instruction'])
        }
        else if(this.currentUser$.email == 'mazida179@gmail.com')
        {
          this.adminGuard = true
         
          this.router.navigate(['/admindashboard'])
        }
    
      }).catch(err =>
        {
          alert(err)
        })
  }


  // Sign out user. 
  signOut()
  {
    return this.afAuth.signOut().then(() =>
    {
      localStorage.removeItem('user')
      alert('Logged out')
    })
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AddquestionComponent } from './admin/addquestion/addquestion.component';

// Mat table. 
import {MatTableModule} from '@angular/material/table';

//Google login, firebase
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { environment } from 'src/environments/environment';
import { ChangebgDirective } from './changebg.directive';
import { FooterComponent } from './components/footer/footer.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { AdminsidebarComponent } from './admin/adminsidebar/adminsidebar.component';
import { ConfirmComponent } from './admin/confirm/confirm.component';
import { EditComponent } from './admin/edit/edit.component';
import { DatabasetableComponent } from './admin/databasetable/databasetable.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { InstructionComponent } from './user-components/instruction/instruction.component';
import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { QuestionListComponent } from './user-components/question-list/question-list.component';
import { CustomizeComponent } from './user-components/customize/customize.component';
import { ReviewComponent } from './user-components/review/review.component';
import { ScorecardComponent } from './user-components/scorecard/scorecard.component';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { StartquizComponent } from './user-components/startquiz/startquiz.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    AdmindashboardComponent,
    FooterComponent,
    AddquestionComponent,
    AdminsidebarComponent,
    EditComponent,
    ConfirmComponent,
    DatabasetableComponent,
    PageNotFoundComponent,
    ChangebgDirective,
    InstructionComponent,
    ProfileHeaderComponent,
    QuestionListComponent,
    CustomizeComponent,
    ReviewComponent,
    ScorecardComponent,
    AccessDeniedComponent,
    StartquizComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

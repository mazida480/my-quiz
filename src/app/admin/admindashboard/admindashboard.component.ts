import { Component } from '@angular/core';
import { DataapiService } from 'src/app/services/api/dataapi.service';
import { Question } from 'src/app/models/question';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent 
{
  date = new Date().toDateString()
  time = new Date().toLocaleTimeString()
  searchById = false 
  id:string = ""
  result = new Question("", "", "", "", "", "", "", "",)

  constructor(private dataapi:DataapiService)
  {
    this.dataapi.getOneQues(this.id).subscribe(res =>
      {
        this.result = res 
      })
  }

  viewById()
  {
    this.searchById = !this.searchById
  }

}

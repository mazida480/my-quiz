import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-adminsidebar',
  templateUrl: './adminsidebar.component.html',
  styleUrls: ['./adminsidebar.component.css']
})
export class AdminsidebarComponent 
{
    photoURL!:string
    displayName!:string

    constructor(private authService:AuthService)
    {
      this.photoURL = this.authService.currentUser$.photoURL
      this.displayName = this.authService.currentUser$.displayName
    }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabasetableComponent } from './databasetable.component';

describe('DatabasetableComponent', () => {
  let component: DatabasetableComponent;
  let fixture: ComponentFixture<DatabasetableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatabasetableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatabasetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

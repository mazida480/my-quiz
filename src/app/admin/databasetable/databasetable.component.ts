import { Component, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-databasetable',
  templateUrl: './databasetable.component.html',
  styleUrls: ['./databasetable.component.css']
})
export class DatabasetableComponent 
{
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;
  
  questions:any ;
  allTableHeaders:any ;
  requiredTableHeader:string[] = ["Question", "Answer", "Category", "Level", "Edit", "Delete"]
  constructor(private dataapi:DataapiService )
  {
    this.dataapi.getAllQues().subscribe(result =>
      {
        this.questions = result

      })

    }
}


import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router} from '@angular/router';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent 
{
  questions:{} = {}
  id:string|null =""
  resultdata:number = -1

  constructor(private dataapi:DataapiService, private activeroute:ActivatedRoute, private router:Router)
  {
    this.activeroute.paramMap.subscribe((param:ParamMap) =>
    {
      console.log(param)
      this.id = param.get('id')
    })

  }

  okay()
  {
    if(typeof this.id == 'string')
    {
      this.dataapi.deleteQues(this.id).subscribe(result =>
        {
          this.resultdata = result
          console.log(this.resultdata)
        })
    }
    if(this.resultdata > 0)
    {
      alert("Record Deleted." + this.resultdata)
      this.router.navigate(["/databasetable"])
    }
   
  }

  cancel()
  {
    this.router.navigate(["/databasetable"])
  }

}

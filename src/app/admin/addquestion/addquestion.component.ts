import { Component } from '@angular/core';
import { DataapiService } from '../../services/api/dataapi.service';
import { Question } from '../../models/question';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addquestion',
  templateUrl: './addquestion.component.html',
  styleUrls: ['./addquestion.component.css']
})
export class AddquestionComponent 
{
  question = new Question("", "", "", "", "", "", "","")
  category = ["History", "Geography", "Politics", "Maths", "Biology"] 
  level = ["Beginner", "Intermediate", "Advanced"]
  options = ["opta", "optb", "optc", "optd"]

  constructor(private dataapi:DataapiService, private router:Router){}

  addQues()
  {
    this.dataapi.postQues(this.question).subscribe(res =>
      {
        if(!res)
        {
          alert("Error occured")
        }
        else
        {
          alert("You have sent\n"+(res))
          this.router.navigate(['/admindashboard'])
        }
      })
  }
  
}

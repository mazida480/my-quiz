import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DataapiService } from 'src/app/services/api/dataapi.service';
import { Question } from 'src/app/models/question';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent 
{
  question:any = new Question("", "", "", "", "", "", "", "")
  category:string[] = ["History", "Geography", "Politics", "Maths", "Biology"]
  level:string[] = ["Beginner", "Intermediate", "Advanced", ]
  options:string[] = ["opta", "optb", "optc", "optd"]
  id:any = ''

  constructor(private dataapi:DataapiService, private activeroute:ActivatedRoute, private router:Router)
  {
    this.activeroute.paramMap.subscribe((param:ParamMap) =>
    {
      console.log(param)
      this.id = param.get('id')
    })

    if(typeof this.id == 'string')
    {
      this.dataapi.getOneQues(this.id).subscribe(onedata =>
      {
        console.log(onedata)
        this.question = onedata 
      })
    }
  }

  editThisQues()
  {
    this.dataapi.updateQues(this.question, this.id).subscribe(editedQues =>
      {
        alert("Question Edited.")
        console.log(editedQues)
        this.question = editedQues
        this.router.navigate(["/admindashboard"])
      })
  }
}

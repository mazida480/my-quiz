import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appChangebg]'
})
export class ChangebgDirective 
{
  @Input() isCorrect:boolean = false


  constructor(private el:ElementRef, private renderer:Renderer2) { }

  @HostListener('click') selectAnswer()
  {
    if(this.isCorrect)
    {
      this.renderer.setStyle(this.el.nativeElement, 'background', 'green')
      this.renderer.setStyle(this.el.nativeElement, 'color', 'white')
      this.renderer.setStyle(this.el.nativeElement, 'border', '2px solid grey')

    }
    else
    {
      this.renderer.setStyle(this.el.nativeElement, 'background', 'red')
      this.renderer.setStyle(this.el.nativeElement, 'color', 'white')
      this.renderer.setStyle(this.el.nativeElement, 'border', '2px solid grey')
    }
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddquestionComponent } from './admin/addquestion/addquestion.component';
import { AuthGuard } from './guard/auth.guard';
import { ConfirmComponent } from './admin/confirm/confirm.component';
import { EditComponent } from './admin/edit/edit.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { AdminsidebarComponent } from './admin/adminsidebar/adminsidebar.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { QuestionListComponent } from './user-components/question-list/question-list.component';
import { CustomizeComponent } from './user-components/customize/customize.component';
import { ReviewComponent } from './user-components/review/review.component';
import { ScorecardComponent } from './user-components/scorecard/scorecard.component';
import { InstructionComponent } from './user-components/instruction/instruction.component';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { AdminGuardGuard } from './guard/admin-guard.guard';
import { StartquizComponent } from './user-components/startquiz/startquiz.component';

const routes: Routes = 
[
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path:'edit/:id', component:EditComponent, title:'edit'},
  {path:'confirm/:id', component:ConfirmComponent, title:'confirm'},
  {path:'home', component:HomeComponent,title:'home'},
  {path:'instruction', component:InstructionComponent, title:'Instruction'},
  {path:'customize', component:CustomizeComponent, title:'Customize'},
  {path:'startquiz', component:StartquizComponent, title:'Startquiz'},
  {path:'review', component:ReviewComponent, title:'Review'},
  {path:'score', component:ScorecardComponent,  title:'Score'},
  {path:'questionlist', component:QuestionListComponent,  title:'Questions'},
  {path:'admindashboard', component:AdmindashboardComponent, canActivate:[AdminGuardGuard], title:'admindashboard'},
  {path:'addquestion', component:AddquestionComponent,canActivate:[AdminGuardGuard], title:'addquestion'},
  {path:'adminsidebar', component:AdminsidebarComponent,canActivate:[AdminGuardGuard], title:'adminsidebar'},
  {path:'denied', component:AccessDeniedComponent, title:'Denied'},
  {path:'**', component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

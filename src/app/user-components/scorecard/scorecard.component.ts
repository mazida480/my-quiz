import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-scorecard',
  templateUrl: './scorecard.component.html',
  styleUrls: ['./scorecard.component.css']
})
export class ScorecardComponent
{
  selectedAns!:[]
  score:any
  percent!:number
  remark:string = ''

  constructor(private apiService:DataapiService, private router:Router)
  {
    this.selectedAns = this.apiService.selectedAnswer.value
    this.score = this.apiService.score.value
    this.percent = (this.score.correctAns/this.score.incorrectAns)*100
    if(this.percent >= 70)
    {
      this.remark = "Excellent!"
    }
    else if(this.percent >= 50)
    {
      this.remark = "Good!"
    }
    else{
      this.remark = "Poor!"
    }
  }

  goToReview()
  {
    this.router.navigate(['/review'])
  }
}

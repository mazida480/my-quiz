import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from 'src/app/models/question';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent
{
  date:Date = new Date()
  startTime:string = new Date().toLocaleTimeString()
  addHours = this.date.setHours(this.date.getHours()+2)
  endTime = this.date.toLocaleTimeString()
  timer:string = '' 
  questionlist:Question[] = []
  currentQuestion:number = 0 
  filter:any
  isCorrect:boolean[] = []
  score = {correctAns:0, incorrectAns:0, points:0, qnum:0}
  progress:string = '0'


  constructor(private apiService:DataapiService, private router:Router)
  {
    this.filter = this.apiService.filterVal.value
    this.score.qnum = this.apiService.filterVal.value.qnum
       this.apiService.filterByCatAndLevel(this.filter.category, this.filter.level).subscribe(res =>
      {
        this.questionlist = res 
        // this.questionlist.length = this.apiService.filterVal.value.qnum
       
      })
      setInterval(() =>
        {
          const now = new Date().getTime()
          const difference = this.date.getTime() - now 
      
          // Find hr, min, sec. 
          const hour = Math.floor((difference % (1000*60*60*24)) / (1000*60*60)) ;
          const min = Math.floor((difference % (1000*60*60)) / (1000*60)) ;
          const sec = Math.floor((difference % (1000*60)) / (1000)) ;
      
          this.timer = `${hour} : ${min} : ${sec}`
      
          if(difference < 0)
          {
              alert("Time Completed.")
          }
      
        }, 1000)
  }

  nextQuestion()
  {
    this.currentQuestion++
  }

  prevQuestion()
  {
    this.currentQuestion--
  }

  select(option:string)
  {
    if(this.isCorrect.length != this.questionlist.length)
    {
      if(this.isCorrect.length == this.currentQuestion)
      {
        const result = (this.questionlist[this.currentQuestion].ans == option)
        this.isCorrect.push(result)
        if(result)
        {
          this.score.correctAns++
          this.score.points += 10
          this.calcPercent()
        }
        else
        {
          this.score.incorrectAns++
        }
      }
      else
      {
        alert("You cannot selece more than once skip a question.")
      }
    }
  }

  goToQuestion(i:number)
  {
    this.currentQuestion = i 
  }

  submit()
  {
    this.apiService.selectedAnswer.next(this.isCorrect)
   
    this.apiService.score.next(this.score)
    this.router.navigate(['/score'])
  }

  goBack()
  {
    this.router.navigate(['/customize'])
  }

  calcPercent()
  {
    const percent = ((this.score.correctAns / this.questionlist.length)*100).toString()
    this.progress = percent
    console.log(percent)
    
  }

 countDownTimer()
{
    // Today's data. 
    const now = new Date().getTime()
    const difference = this.date.getTime() - now 

    // Find hr, min, sec. 
    const hour = Math.floor((difference % (1000*60*60*24)) / (1000*60*60)) ;
    const min = Math.floor((difference % (1000*60*60)) / (1000*60)) ;
    const sec = Math.floor((difference % (1000*60)) / (1000)) ;

    this.timer = hour+'Hr'+min+'mins'+sec+'sec'

    if(difference < 0)
    {
        alert("Time Completed.")
    }

    

}
}


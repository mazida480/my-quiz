import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-startquiz',
  templateUrl: './startquiz.component.html',
  styleUrls: ['./startquiz.component.css']
})
export class StartquizComponent 
{
  filter!:any 
  category!:string
  level!:string
  qnum!:number

  constructor(private apiService:DataapiService, private router:Router)
  {
    this.filter = this.apiService.filterVal.value
    this.category = this.filter.category
    this.level = this.filter.level
    this.qnum = this.filter.qnum
  }

  nextPage()
  {
    this.router.navigate(['/questionlist'])
  }

  goBack()
  {
    this.router.navigate(['/startquiz'])
  }
}

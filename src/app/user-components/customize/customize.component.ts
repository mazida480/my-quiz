import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-customize',
  templateUrl: './customize.component.html',
  styleUrls: ['./customize.component.css']
})
export class CustomizeComponent
{
  customize = {category:'', level:'', qnum:5, }
  category:string[] = ["Maths", "Biology", "Politics"]
  level:string[] = ["Beginner", "Intermediate", "Advanced"]

  constructor(private apiService:DataapiService, private router:Router)
  {}

  nextPage()
  {
    this.apiService.filterVal.next(this.customize)
    this.router.navigate(['/startquiz'])
  }
}

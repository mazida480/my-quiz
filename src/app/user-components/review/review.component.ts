import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from 'src/app/models/question';
import { DataapiService } from 'src/app/services/api/dataapi.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent 
{
  filter:any 
  questionlist:Question[] = []
  currentQuestion:number = 0
  isCorrect:any = []
  isBackground!:any

  constructor(private apiService:DataapiService, private router:Router)
  {
    this.filter = this.apiService.filterVal.value
    this.apiService.filterByCatAndLevel(this.filter.category, this.filter.level).subscribe(res =>
      {
        this.questionlist = res 
        this.isCorrect = this.apiService.selectedAnswer.value
        this.isBackground = this.isCorrect[this.currentQuestion]
      })
  }

  prevQuestion()
  {
    this.currentQuestion--
  }

  nextQuestion()
  {
    this.isBackground = this.isCorrect[this.currentQuestion]
    console.log(this.isBackground)
    this.currentQuestion++
  }
}

import { Component } from '@angular/core';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngquizapp';

  hideLogginHeader!:boolean
  constructor(private authService:AuthService)
  {
    console.log(this.hideLogginHeader)
  }
}

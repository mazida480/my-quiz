export class Question 
{
    
    constructor(
        public ques:string, 
        public opta:string, 
        public optb:string, 
        public optc:string, 
        public optd:string, 
        public cat:string, 
        public ans:string, 
        public level:string)
    {}
}
